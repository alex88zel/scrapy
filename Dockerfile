FROM python:3.8

ENV LOG_FORMAT="json"

COPY . /usr/src/app
RUN pip install --no-cache /usr/src/app && \
    rm -rf /usr/src/app

COPY scrapy_spider /scrapy_spider
WORKDIR /scrapy_spider

CMD ["scrapy", "crawl", "quotes_spider", "-t", "$LOG_FORMAT"]
