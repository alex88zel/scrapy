# Scrapy
This is an example CI pipeline for dockerized [Scrapy web scraper](https://github.com/scrapy/scrapy#scrapy)  
In this example we will:
1. Run CI using GitLab:
    1. Run tests (unit, lint)
    1. Build & Publish PyPi package
    1. Build & Push a Docker image

The Docker image will be configured to scrape a target site continuously,  
results will be logged to stdout which can be collected by various tools.  
The scraped website is [http://quotes.toscrape.com/](http://quotes.toscrape.com/), 
based on the tutorial [here](https://www.accordbox.com/blog/scrapy-tutorial-5-how-create-simple-scrapy-spider/).  

This project was cloned from [Scrapy GitHub](https://github.com/scrapy/scrapy), the modified files are:
1. [Dockerfile](Dockerfile)
1. [.gitlab-ci.yml](.gitlab-ci.yml)
1. [scrapy_spider](scrapy_spider)

## Making changes
After making any changes a version bump is required, [bumpversion](https://pypi.org/project/bumpversion/) is used for this  
```
pip install bumpversion
bumpversion patch
git push
```

## Running locally
Once the CI pipeline finishes we can use the created artifacts:
* PyPi:
    1. `pip install Scrapy --extra-index-url https://gitlab.com/api/v4/projects/21418589/packages/pypi/simple`
* Docker:
    1. `docker pull registry.gitlab.com/alex88zel/scrapy`
    1. `docker run registry.gitlab.com/alex88zel/scrapy`
